#pragma once
#include <iostream>
#include <fstream>
#include <Eigen/Geometry>
#include <srrg_types/types.hpp>

using namespace srrg_core;

class MetricEuRoC {

public:

  struct PositionMeasurement {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    PositionMeasurement(const double& timestamp_seconds_,
                        const Eigen::Vector3d& position_): timestamp_seconds(timestamp_seconds_),
                                                           position(position_) {}
    PositionMeasurement(): timestamp_seconds(0),
                           position(Eigen::Vector3d::Zero()) {}
    double timestamp_seconds;
    Eigen::Vector3d position;
  };
  typedef std::pair<PositionMeasurement, PositionMeasurement> PositionMeasurementPair;

  static const double getAbsoluteTranslationRootMeanSquaredError(const std::vector<PositionMeasurementPair> position_correspondences_) {

    //ds compute absolute squared errors
    std::vector<double> squared_errors_translation_absolute(0);
    for (const PositionMeasurementPair& position_correspondence: position_correspondences_) {

      //ds compute squared errors between frames
      squared_errors_translation_absolute.push_back((position_correspondence.first.position-position_correspondence.second.position).squaredNorm());
    }

    //ds compute RMSE
    double root_mean_squared_error_translation_absolute = 0;
    for (const double& squared_error: squared_errors_translation_absolute) {
      root_mean_squared_error_translation_absolute += squared_error;
    }
    root_mean_squared_error_translation_absolute /= squared_errors_translation_absolute.size();
    root_mean_squared_error_translation_absolute = std::sqrt(root_mean_squared_error_translation_absolute);
    squared_errors_translation_absolute.clear();

    //ds done
    return root_mean_squared_error_translation_absolute;
  }


  static const double getRelativeTranslationMeanError(const std::vector<PositionMeasurementPair> position_correspondences_) {

    //ds compute relative errors
    std::vector<double> errors_translation_relative(0);
    PositionMeasurementPair position_correspondence_previous;
    bool previous_available = false;
    for (const PositionMeasurementPair& position_correspondence: position_correspondences_) {
      if (previous_available) {
        const Eigen::Vector3d translation_estimate     = position_correspondence.first.position-position_correspondence_previous.first.position;
        const Eigen::Vector3d translation_ground_truth = position_correspondence.second.position-position_correspondence_previous.second.position;
        errors_translation_relative.push_back((translation_estimate-translation_ground_truth).norm());
      } else {
        previous_available = true;
      }
      position_correspondence_previous = position_correspondence;
    }

    //ds compute error
    double mean_error_translation_relative = 0;
    for (const double& error: errors_translation_relative) {
      mean_error_translation_relative += error;
    }
    mean_error_translation_relative /= errors_translation_relative.size();
    errors_translation_relative.clear();

    //ds done
    return mean_error_translation_relative;
  }

  static const Eigen::Vector3d getInterpolatedPositionLinear(const PositionMeasurement& ground_truth_previous_,
                                                             const PositionMeasurement& ground_truth_next_,
                                                             const PositionMeasurement& measurement_) {

    //ds interpolate to next
    const double timestamp_difference_seconds_ground_truth = ground_truth_next_.timestamp_seconds-ground_truth_previous_.timestamp_seconds;
    const double timestamp_difference_seconds = measurement_.timestamp_seconds-ground_truth_previous_.timestamp_seconds;

    //ds compute interpolated reference measurement
    const Eigen::Vector3d position_interpolated = ground_truth_previous_.position +
                                                  timestamp_difference_seconds/timestamp_difference_seconds_ground_truth*
                                                  (ground_truth_next_.position-ground_truth_previous_.position);
    return position_interpolated;
  }

  static const std::vector<PositionMeasurement> loadPosesTUM(const std::string& file_name_trajectory_tum_) {

    //ds open stream
    std::ifstream input_stream(file_name_trajectory_tum_);
    if (!input_stream.good() || !input_stream.is_open()) {
      std::cerr << "MetricEuRoC::loadPosesTUM|ERROR: unable to open: '" << file_name_trajectory_tum_ << "'" << std::endl;
      throw std::runtime_error("MetricEuRoC::loadPosesTUM");
    }

    std::vector<PositionMeasurement> positions;
    std::string buffer_line;
    while (std::getline(input_stream, buffer_line)) {

      //ds get line to a string stream object
      std::istringstream stringstream(buffer_line);

      //ds possible values
      double timestamp_seconds = 0;
      double translation_x = 0;
      double translation_y = 0;
      double translation_z = 0;
      double quaternion_w  = 0;
      double quaternion_x  = 0;
      double quaternion_y  = 0;
      double quaternion_z  = 0;

      //ds parse the full line and check for failure
      if (!(stringstream >> timestamp_seconds >> translation_x >> translation_y >> translation_z
                                              >> quaternion_x >> quaternion_y >> quaternion_z >> quaternion_w)) {
        std::cerr << "MetricEuRoC::loadPosesTUM|ERROR: unable to parse pose lines" << std::endl;
        input_stream.close();
        throw std::runtime_error("MetricEuRoC::loadPosesTUM");
      }

      //ds set pose value
      Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());
      pose.translation() = Eigen::Vector3d(translation_x, translation_y, translation_z);
      pose.linear()      = Eigen::Quaterniond(quaternion_w, quaternion_x, quaternion_y, quaternion_z).toRotationMatrix();

      //ds store position
      positions.push_back(PositionMeasurement(timestamp_seconds, pose.translation()));
    }
    input_stream.close();
    return positions;
  }

  static const std::vector<PositionMeasurement> loadPosesASL(const std::string& file_name_trajectory_asl_) {

    //ds open stream
    std::ifstream input_stream(file_name_trajectory_asl_);
    if (!input_stream.good() || !input_stream.is_open()) {
      std::cerr << "MetricEuRoC::loadPosesASL|ERROR: unable to open: '" << file_name_trajectory_asl_ << "'" << std::endl;
      throw std::runtime_error("MetricEuRoC::loadPosesASL");
    }

    std::vector<PositionMeasurement> positions;
    std::string buffer_line;
    while (std::getline(input_stream, buffer_line)) {

        //ds skip comment lines
        if (buffer_line[0] == '#') {
          continue;
        }

        //ds parse control
        std::string::size_type index_begin_item = 0;
        std::string::size_type index_end_item   = 0;

        //ds parse timestamp
        index_end_item = buffer_line.find(",", index_begin_item);
        const uint64_t timestamp_nanoseconds = std::atol(buffer_line.substr(index_begin_item, index_end_item).c_str());
        const double timestamp_seconds       = timestamp_nanoseconds/1e9;
        index_begin_item = index_end_item+1;

        //ds position buffer
        Eigen::Isometry3d pose(Eigen::Isometry3d::Identity());
        for (uint32_t row = 0; row < 3; ++row) {
          index_end_item = buffer_line.find(",", index_begin_item);
          pose.translation()(row) = std::strtod(buffer_line.substr(index_begin_item, index_end_item-index_begin_item).c_str(), 0);
          index_begin_item = index_end_item+1;
        }

        //ds add to buffer
        positions.push_back(PositionMeasurement(timestamp_seconds, pose.translation()));
      }
    input_stream.close();
    return positions;
  }

  static const std::vector<PositionMeasurementPair> getCorrespondences(std::vector<PositionMeasurement>& estimates_,
                                                                       const std::vector<PositionMeasurement>& ground_truth_) {

    //ds objectives
    std::vector<PositionMeasurementPair> position_correspondences;
    Eigen::Vector3d position_shift(Eigen::Vector3d::Zero());

    //ds for each measurement
    for (uint64_t index_estimate = 0; index_estimate < estimates_.size(); ++index_estimate) {
      PositionMeasurement& estimate = estimates_[index_estimate];

      //ds find closest ground truth point - bruteforce
      double timestamp_difference_seconds_best = 1;
      uint32_t index_best                      = 0;
      for (uint64_t index_ground_truth = 0; index_ground_truth < ground_truth_.size(); ++index_ground_truth) {
        const double timestamp_difference_seconds = std::fabs(estimate.timestamp_seconds-ground_truth_[index_ground_truth].timestamp_seconds);
        if (timestamp_difference_seconds < timestamp_difference_seconds_best) {
          timestamp_difference_seconds_best = timestamp_difference_seconds;
          index_best = index_ground_truth;
        }
      }

      //ds skip until we arrive at the ground truth timestamp
      if (index_best == 0) {
        continue;
      }

      //ds solution
      PositionMeasurement ground_truth_interpolated(estimate.timestamp_seconds, estimate.position);

      //ds interpolation: check if before the system
      if (ground_truth_[index_best].timestamp_seconds < estimate.timestamp_seconds) {

        //ds interpolate to next
        ground_truth_interpolated.position = getInterpolatedPositionLinear(ground_truth_[index_best], ground_truth_[index_best+1], estimate);
      } else {

        //ds interpolate from previous
        ground_truth_interpolated.position = getInterpolatedPositionLinear(ground_truth_[index_best-1], ground_truth_[index_best], estimate);
      }

      //ds for the first measurement - compute starting point offset
      if (index_estimate == 0) {
        position_shift = ground_truth_interpolated.position;
      }

      //ds adjust position
      estimate.position += position_shift;

      //ds save the first correspondence
      position_correspondences.push_back(std::make_pair(estimate, ground_truth_interpolated));
    }
    return position_correspondences;
  }

  static void alignCorrespondencesICP(std::vector<PositionMeasurementPair>& correspondences_) {

    //ds objective
    Eigen::Isometry3d transform_estimate_to_ground_truth(Eigen::Isometry3d::Identity());

    //ds ICP configuration
    const uint32_t number_of_iterations = 100;
    const double maximum_error_kernel   = 0.1; //ds (m^2)

    //ds ICP running variables
    Matrix6d H(Matrix6d::Zero());
    Vector6d b(Vector6d::Zero());

    //ds perform least squares optimization
    for (uint32_t iteration = 0; iteration < number_of_iterations; ++iteration) {

      //ds initialize setup
      H.setZero();
      b.setZero();
      uint32_t number_of_inliers = 0;
      double total_error_squared = 0;

      //ds for all SLAM trajectory poses
      for (const PositionMeasurementPair& position_correspondence: correspondences_) {

        //ds compute current error
        const Eigen::Vector3d& measured_point_in_reference = position_correspondence.second.position;
        const Eigen::Vector3d sampled_point_in_reference   = transform_estimate_to_ground_truth*position_correspondence.first.position;
        const Eigen::Vector3d error                        = sampled_point_in_reference-measured_point_in_reference;

        //ds update chi
        const double error_squared = error.transpose()*error;

        //ds check if outlier
        double weight = 1.0;
        if (error_squared > maximum_error_kernel) {
          weight = maximum_error_kernel/error_squared;
        } else {
          ++number_of_inliers;
        }
        total_error_squared += error_squared;

        //ds get the jacobian of the transform part = [I -2*skew(T*modelPoint)]
        Matrix3_6d jacobian;
        jacobian.block<3,3>(0,0).setIdentity();
        jacobian.block<3,3>(0,3) = -2*skew(sampled_point_in_reference);

        //ds precompute transposed
        const Matrix6_3d jacobian_transposed(jacobian.transpose());

        //ds accumulate
        H += weight*jacobian_transposed*jacobian;
        b += weight*jacobian_transposed*error;
      }

      //ds solve the system and update the estimate
      transform_estimate_to_ground_truth = srrg_core::v2t(static_cast<const Vector6d&>(H.ldlt().solve(-b)))*transform_estimate_to_ground_truth;

      //ds enforce rotation symmetry
      const Eigen::Matrix3d rotation   = transform_estimate_to_ground_truth.linear();
      Eigen::Matrix3d rotation_squared = rotation.transpose( )*rotation;
      rotation_squared.diagonal().array()     -= 1;
      transform_estimate_to_ground_truth.linear() -= 0.5*rotation*rotation_squared;
    }

    //ds compute optimal poses
    for (std::pair<PositionMeasurement, PositionMeasurement>& position_correspondence: correspondences_) {
      position_correspondence.first.position = transform_estimate_to_ground_truth*position_correspondence.first.position;
    }
  }
};
