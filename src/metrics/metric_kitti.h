#pragma once
//ds THIS CODE WAS CREATED BASED ON: http://kitti.is.tue.mpg.de/kitti/devkit_odometry.zip
//ds minimally modified to avoid C++11 warnings and provided a brief result dump to stdout

#include <iostream>
#include <limits>
#include <fstream>
#include <Eigen/Geometry>

class MetricKITTI {

//ds exported types
public:

  typedef Eigen::Matrix<float, 4, 4> Matrix4f;
  typedef std::vector<Matrix4f, Eigen::aligned_allocator<Matrix4f>> PoseVector;
  struct ErrorKITTI {
    ErrorKITTI(const float& relative_error_translation_,
               const float& error_rotation_degrees_per_meter_,
               const float& segment_length_meters_): relative_error_translation(relative_error_translation_),
                                                     error_rotation_degrees_per_meter(error_rotation_degrees_per_meter_),
                                                     segment_length_meters(segment_length_meters_) {}
    ErrorKITTI(): relative_error_translation(0),
                  error_rotation_degrees_per_meter(0),
                  segment_length_meters(0) {}

    float relative_error_translation;
    float error_rotation_degrees_per_meter;
    float segment_length_meters;
  };

//ds functionality
public:

  static PoseVector loadPoses(const std::string& filename_poses_) {
    PoseVector poses(0);

    //ds going modern
    std::ifstream pose_file(filename_poses_, std::ifstream::in);

    //ds grab a line from the ground truth
    std::string buffer_line;
    while (std::getline(pose_file, buffer_line)) {

      //ds get it to a stringstream
      std::istringstream buffer_stream(buffer_line);

      //ds information fields (KITTI format)
      Matrix4f pose(Matrix4f::Identity());
      for (uint8_t u = 0; u < 3; ++u) {
        for (uint8_t v = 0; v < 4; ++v) {
          buffer_stream >> pose(u,v);
        }
      }
      poses.push_back(pose);
    }

    //ds done
    pose_file.close();
    return poses;
  }

  static std::vector<float> trajectoryDistances (const PoseVector &poses) {
    std::vector<float> dist;
    dist.push_back(0);
    for (std::size_t i=1; i<poses.size(); i++) {
      Matrix4f P1 = poses[i-1];
      Matrix4f P2 = poses[i];
      float dx = P1(0,3)-P2(0,3);
      float dy = P1(1,3)-P2(1,3);
      float dz = P1(2,3)-P2(2,3);
      dist.push_back(dist[i-1]+sqrt(dx*dx+dy*dy+dz*dz));
    }
    return dist;
  }

  static int32_t getLastFrameFromSegmentLength(const std::vector<float> &dist,int32_t first_frame,float len) {
    for (std::size_t i=first_frame; i<dist.size(); i++)
      if (dist[i]>dist[first_frame]+len)
        return i;
    return -1;
  }

  static inline float rotationError(const Matrix4f &pose_error) {
    float a = pose_error(0,0);
    float b = pose_error(1,1);
    float c = pose_error(2,2);
    float d = 0.5*(a+b+c-1.0);
    return std::acos(std::max(std::min(d,1.0f),-1.0f));
  }

  static inline float translationError(const Matrix4f &pose_error) {
    float dx = pose_error(0,3);
    float dy = pose_error(1,3);
    float dz = pose_error(2,3);
    return sqrt(dx*dx+dy*dy+dz*dz);
  }

  static std::pair<float, float> computeAverageErrorTranslationAndRotation(const PoseVector& poses_ground_truth_, const PoseVector& poses_result_) {

    //ds translation errors to evaluate on different segment lengths
    std::vector<ErrorKITTI> errors(0);

    //ds pre-compute distances (from ground truth as reference)
    std::vector<float> distances_ground_truth = trajectoryDistances(poses_ground_truth_);

    //ds step size (10 equals 1 second since KITTI was recorded at 10Hz)
    const int32_t step_size = 10;

    //ds segments to check
    std::vector<float> segments = {100, 200, 300, 400, 500, 600, 700, 800};

    //ds for all start positions do
    for (int32_t index_first_frame = 0; index_first_frame < static_cast<int32_t>(poses_ground_truth_.size()); index_first_frame += step_size) {

      //ds for all segment lengths do
      for (const float& segment_length: segments) {

        //ds compute last frame
        const int32_t index_last_frame = getLastFrameFromSegmentLength(distances_ground_truth, index_first_frame, segment_length);

        //ds continue, if sequence not long enough
        if (index_last_frame == -1) {continue;}

        //ds compute pose error
        const Matrix4f pose_delta_gt     = poses_ground_truth_[index_first_frame].inverse()*poses_ground_truth_[index_last_frame];
        const Matrix4f pose_delta_result = poses_result_[index_first_frame].inverse()*poses_result_[index_last_frame];
        const Matrix4f pose_error        = pose_delta_result.inverse()*pose_delta_gt;

        //ds update error
        errors.push_back(ErrorKITTI(translationError(pose_error)/segment_length, rotationError(pose_error)/segment_length*(180/M_PI), segment_length));
      }
    }

    //ds for all segment lengths do
    float total_error_translation         = 0;
    float total_error_rotation            = 0;
    uint32_t number_of_evaluated_segments = 0;
    for (const float& segment_length: segments) {

      //ds measurements
      float accumulated_error_translation = 0;
      float accumulated_error_rotation    = 0;
      float number_of_measurements        = 0;

      //ds for all errors check if they are in the evaluable range
      for (const ErrorKITTI& error: errors) {
        if (std::fabs(error.segment_length_meters-segment_length) < 1.0) {
          accumulated_error_translation += error.relative_error_translation;
          accumulated_error_rotation    += error.error_rotation_degrees_per_meter;
          ++number_of_measurements;
        }
      }

      //ds we require at least 3 measurements
      if (number_of_measurements > 2) {
        total_error_translation += accumulated_error_translation/number_of_measurements;
        total_error_rotation    += accumulated_error_rotation/number_of_measurements;
        ++number_of_evaluated_segments;
      }
    }

    //ds return with average
    return std::make_pair(total_error_translation/number_of_evaluated_segments, total_error_rotation/number_of_evaluated_segments);
  }
};
