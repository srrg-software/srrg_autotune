#include "system/slam_assembly.h"
#include "metrics/metric_euroc.h"

#define TO_STREAM(PARAMETER_DESCRIPTION_, PARAMETER_, STREAM_) \
  STREAM_ << PARAMETER_DESCRIPTION_ << ": " << PARAMETER_ << std::endl;

#define TO_STREAM_RANGE(PARAMETER_DESCRIPTION_, PARAMETER_, PARAMETER_VECTOR_, STREAM_) \
  STREAM_ << PARAMETER_DESCRIPTION_ << ": " << PARAMETER_ << " [" << PARAMETER_VECTOR_.front() << ", " << PARAMETER_VECTOR_.back() << "]" << std::endl;

using namespace proslam;
int32_t main(int32_t argc_, char** argv_) {

  //ds simple input validation
  if (argc_ < 3) {
    std::cerr << "ERROR: invalid call - use ./tune_proslam_euroc <result_file.txt> <euroc_sequence> <euroc_sequence> .." << std::endl;
    std::cerr << "                     e.g. ./tune_proslam_euroc results.txt MH_01_easy MH_02_easy" << std::endl;
    return 0;
  }

  //ds grab result file name
  const std::string result_file_name = argv_[1];
  std::cerr << "result file: '" << result_file_name << "'" << std::endl;

  //ds parse sequences to evaluate
  std::vector<std::string> sequences;
  while (sequences.size() < static_cast<std::vector<Identifier>::size_type>(argc_-2)) {
    sequences.push_back(argv_[sequences.size()+2]);
  }

  //ds number of sequences to check
  const Count number_of_sequences = sequences.size();

  //ds buffer ground truth poses for all datasets
  std::vector<std::vector<MetricEuRoC::PositionMeasurement>> positions_ground_truth;
  for (const std::string& sequence: sequences) {

    //ds generate file name
    char buffer_filename_ground_truth[32];
    std::sprintf(buffer_filename_ground_truth, "%s.csv", sequence.c_str());
    positions_ground_truth.push_back(MetricEuRoC::loadPosesASL(buffer_filename_ground_truth));
    std::printf("loaded ground truth positions for '%s': %6lu\n", buffer_filename_ground_truth, positions_ground_truth.back().size());
  }

  //ds objective value
  real error_rmse_best = 100;

  //ds manual parameters
  const bool option_recover_landmarks               = true;
  const Count maximum_number_of_landmark_recoveries = 10;
  const bool enable_keypoint_binning                = true;
  const bool option_equalize_histogram              = true;

  const uint32_t detector_threshold_maximum        = 100;
  const uint32_t number_of_detectors_vertical      = 1;
  const uint32_t number_of_detectors_horizontal    = 1;

  const uint32_t minimum_track_length_for_landmark_creation = 2;
  const uint32_t minimum_number_of_landmarks_to_track       = 5;

  //ds automated parameters
  std::vector<real> target_number_of_keypoints_tolerances            = {0.1, 0.2, 0.3, 0.4, 0.5};
  std::vector<real> detector_threshold_maximum_changes               = {0.1, 0.2, 0.3, 0.4, 0.5};
  std::vector<uint32_t> minimum_thresholds_distance_tracking_pixels  = {4};
  std::vector<uint32_t> maximum_thresholds_distance_tracking_pixels  = {10};
  std::vector<uint32_t> bin_sizes_pixels                             = {10};
  std::vector<uint32_t> matching_distances_tracking_threshold        = {35};
  std::vector<uint32_t> maximum_matching_distances_triangulation     = {35, 40, 45, 50, 55, 60, 65, 70, 75, 80};
  std::vector<double> dampings                                       = {0};
  std::vector<uint32_t> detector_thresholds                          = {20};

  //ds start sampling for each parameter range
  std::printf("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
  uint64_t number_of_tested_parameters = 0;
  const uint64_t number_of_parameters_to_test = detector_threshold_maximum_changes.size()*
                                                target_number_of_keypoints_tolerances.size()*
                                                minimum_thresholds_distance_tracking_pixels.size()*
                                                maximum_thresholds_distance_tracking_pixels.size()*
                                                bin_sizes_pixels.size()*
                                                maximum_matching_distances_triangulation.size()*
                                                dampings.size()*
                                                matching_distances_tracking_threshold.size()*
                                                detector_thresholds.size();
  std::cerr << "number of configurations to test: " << number_of_parameters_to_test << " .. good luck with that!" << std::endl;
  for (const real& detector_threshold_maximum_change: detector_threshold_maximum_changes) {
  for (const real& target_number_of_keypoints_tolerance: target_number_of_keypoints_tolerances) {
  for (const uint32_t& bin_size_pixels: bin_sizes_pixels) {
  for (const uint32_t& matching_distance_tracking_threshold: matching_distances_tracking_threshold) {
  for (const uint32_t& maximum_matching_distance_triangulation: maximum_matching_distances_triangulation) {
  for (const real& damping: dampings) {
  for (const uint32_t& minimum_threshold_distance_tracking_pixels: minimum_thresholds_distance_tracking_pixels) {
  for (const uint32_t& maximum_threshold_distance_tracking_pixels: maximum_thresholds_distance_tracking_pixels) {
  for (const uint32_t& detector_threshold_minimum: detector_thresholds) {

    //ds current result values
    real error_rmse_accumulated = 0;
    real average_fps            = 0;

    //ds evaluate all datasets with the current parameter configuration (as of now all the txt_io datasets have to be directly in the root folder otherwise the image paths are broken)
    for (Index index = 0; index < number_of_sequences; ++index) {

      //ds get a fresh parameter collection with default parameters
      ParameterCollection* parameters = new ParameterCollection();

      //ds enable stereo mode
      parameters->setMode(CommandLineParameters::TrackerMode::RGB_STEREO);

      //ds use constant velocity motion model
      parameters->tracker_parameters->motion_model = Parameters::MotionModel::CONSTANT_VELOCITY;

      //ds disable relocalization (benchmarking odometry)
      parameters->command_line_parameters->option_disable_relocalization = true;

      //ds manual parameters
      parameters->command_line_parameters->option_recover_landmarks   = option_recover_landmarks;
      parameters->tracker_parameters->enable_landmark_recovery = option_recover_landmarks;
      parameters->tracker_parameters->maximum_number_of_landmark_recoveries = maximum_number_of_landmark_recoveries;
      parameters->tracker_parameters->minimum_number_of_landmarks_to_track  = minimum_number_of_landmarks_to_track;
      parameters->stereo_framepoint_generator_parameters->enable_keypoint_binning = enable_keypoint_binning;
      parameters->stereo_framepoint_generator_parameters->maximum_descriptor_distance_tracking = matching_distance_tracking_threshold;
      parameters->stereo_framepoint_generator_parameters->detector_threshold_minimum = detector_threshold_minimum;
      parameters->stereo_framepoint_generator_parameters->detector_threshold_maximum = detector_threshold_maximum;
      parameters->stereo_framepoint_generator_parameters->number_of_detectors_vertical   = number_of_detectors_vertical;
      parameters->stereo_framepoint_generator_parameters->number_of_detectors_horizontal = number_of_detectors_horizontal;
      parameters->stereo_framepoint_generator_parameters->descriptor_type                = "BRIEF";
      parameters->tracker_parameters->aligner->damping = damping;
      parameters->tracker_parameters->minimum_track_length_for_landmark_creation = minimum_track_length_for_landmark_creation;

      //ds tuning parameters
      parameters->stereo_framepoint_generator_parameters->detector_threshold_maximum_change = detector_threshold_maximum_change;
      parameters->stereo_framepoint_generator_parameters->target_number_of_keypoints_tolerance = target_number_of_keypoints_tolerance;
      parameters->stereo_framepoint_generator_parameters->minimum_projection_tracking_distance_pixels = minimum_threshold_distance_tracking_pixels;
      parameters->stereo_framepoint_generator_parameters->maximum_projection_tracking_distance_pixels = maximum_threshold_distance_tracking_pixels;
      parameters->stereo_framepoint_generator_parameters->maximum_matching_distance_triangulation = maximum_matching_distance_triangulation;
      parameters->stereo_framepoint_generator_parameters->bin_size_pixels = bin_size_pixels;

      //ds generate file name
      char dataset_file_name[32];
      std::sprintf(dataset_file_name, "%s.txt", sequences[index].c_str());

      //ds set command line parameters
      parameters->command_line_parameters->dataset_file_name = dataset_file_name;

      //ds get a fresh proslam assembly (destroyed when leaving the for loop scope)
      SLAMAssembly* slam_system = new SLAMAssembly(parameters);

      //ds load cameras from message file
      slam_system->loadCamerasFromMessageFile();
      std::printf("[%06lu/%06lu] '%s' > ", number_of_tested_parameters, number_of_parameters_to_test, dataset_file_name);
      std::fflush(stdout);

      //ds start message playback - blocks until dataset is completed or aborted
      slam_system->playbackMessageFile();

      //ds obtain trajectory with timestamps
      std::vector<std::pair<real, TransformMatrix3D>> poses(0);
      slam_system->writeTrajectoryWithTimestamps<real>(poses);

      //ds if something was computed
      if (poses.size() > 0) {

        //ds convert it to our metric
        std::vector<MetricEuRoC::PositionMeasurement> positions(poses.size());
        for (uint64_t index_pose = 0; index_pose < poses.size(); ++index_pose) {
          positions[index_pose].timestamp_seconds = poses[index_pose].first;
          positions[index_pose].position = poses[index_pose].second.translation();
        }

        //ds obtain corresponding measurements w.r.t. the ground truth
        std::vector<MetricEuRoC::PositionMeasurementPair> position_correspondences(MetricEuRoC::getCorrespondences(positions, positions_ground_truth[index]));

        //ds align correspondences
        MetricEuRoC::alignCorrespondencesICP(position_correspondences);

        //ds compute errors
        const real& error_rmse = MetricEuRoC::getAbsoluteTranslationRootMeanSquaredError(position_correspondences);
        const real& error_me   = MetricEuRoC::getRelativeTranslationMeanError(position_correspondences);
        std::printf("absolute translation RMSE (m): %6.3f relative ME (m): %6.3f fps: %3.0f\n", error_rmse, error_me, slam_system->currentFPS());
        error_rmse_accumulated += error_rmse;
        average_fps += slam_system->currentFPS();
      } else {
        std::printf("no images processed\n");
      }
      delete slam_system;
      delete parameters;
    }
    ++number_of_tested_parameters;

    //ds check if we got a better result than the best so far
    const real error_rmse = error_rmse_accumulated/number_of_sequences;
    if (error_rmse < error_rmse_best ) {

      //ds update best
      error_rmse_best = error_rmse;
      std::printf("--------------------------------------------------------------------------------------------------------------\n");
      std::printf("found new BEST absolute translation RMSE (m): %6.3f\n", error_rmse_best);
      std::printf("--------------------------------------------------------------------------------------------------------------\n");

      //ds open result file (appending to previous results)
      std::ofstream result_stream(result_file_name, std::ifstream::app);
      if (!result_stream.is_open() || !result_stream.good()) {
        std::cerr << "ERROR: unable to open result file: '" << result_file_name << "'" << std::endl;
      }

      //ds write header
      result_stream << std::endl;
      result_stream << "--------------------------------------------------------------------------------------------------------------" << std::endl;
      result_stream << "     configuration identifier: " << number_of_tested_parameters << std::endl;
      result_stream << "absolute translation RMSE (m): " << error_rmse_best << std::endl;
      result_stream << "                  average FPS: " << average_fps/number_of_sequences << std::endl;
      result_stream << "              EuRoC sequences: ";
      for (const std::string& sequence_name: sequences) {
        result_stream << sequence_name << " ";
      }
      result_stream << std::endl;
      result_stream << std::endl;
      result_stream << "parameters: " << std::endl;
      result_stream << std::endl;

      //ds manual parameters
      TO_STREAM("parameters->stereo_tracker_parameters->enable_landmark_recovery", option_recover_landmarks, result_stream)
      TO_STREAM("parameters->stereo_tracker_parameters->maximum_number_of_landmark_recoveries", maximum_number_of_landmark_recoveries, result_stream)
      TO_STREAM("parameters->stereo_tracker_parameters->minimum_number_of_landmarks_to_track", minimum_number_of_landmarks_to_track, result_stream)
      TO_STREAM("parameters->stereo_tracker_parameters->enable_keypoint_binning", enable_keypoint_binning, result_stream)
      TO_STREAM("parameters->command_line_parameters->option_equalize_histogram", option_equalize_histogram, result_stream)
      TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->matching_distance_tracking_threshold", matching_distance_tracking_threshold, matching_distances_tracking_threshold, result_stream)
      TO_STREAM("parameters->stereo_framepoint_generator_parameters->detector_threshold_initial", detector_threshold_minimum, result_stream)
      TO_STREAM("parameters->stereo_framepoint_generator_parameters->detector_threshold_minimum", detector_threshold_minimum, result_stream)
      TO_STREAM("parameters->stereo_framepoint_generator_parameters->number_of_detectors_vertical", number_of_detectors_vertical, result_stream)
      TO_STREAM("parameters->stereo_framepoint_generator_parameters->number_of_detectors_horizontal", number_of_detectors_horizontal, result_stream)
      TO_STREAM("parameters->stereo_tracker_parameters->minimum_track_length_for_landmark_creation", minimum_track_length_for_landmark_creation, result_stream)
      result_stream << std::endl;

      //ds tuning parameters
      TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->target_number_of_keypoints_tolerance", target_number_of_keypoints_tolerance, target_number_of_keypoints_tolerances, result_stream)
      TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->detector_threshold_maximum_change", detector_threshold_maximum_change, detector_threshold_maximum_changes, result_stream)
      TO_STREAM_RANGE("parameters->stereo_tracker_parameters->minimum_projection_tracking_distance_pixels", minimum_threshold_distance_tracking_pixels, minimum_thresholds_distance_tracking_pixels, result_stream)
      TO_STREAM_RANGE("parameters->stereo_tracker_parameters->maximum_projection_tracking_distance_pixels", maximum_threshold_distance_tracking_pixels, maximum_thresholds_distance_tracking_pixels, result_stream)
      TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->maximum_matching_distance_triangulation", maximum_matching_distance_triangulation, maximum_matching_distances_triangulation, result_stream)
      TO_STREAM_RANGE("parameters->stereo_tracker_parameters->bin_size_pixels", bin_size_pixels, bin_sizes_pixels, result_stream)
      TO_STREAM_RANGE("parameters->stereo_tracker_parameters->aligner->damping", damping, dampings, result_stream)
      result_stream << std::endl;

      //ds done
      result_stream.close();
    }
  }
  }
  }
  }
  }
  }
  }
  }
  }

  //ds done
  return 0;
}
