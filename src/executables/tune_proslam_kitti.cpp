#include "system/slam_assembly.h"
#include "metrics/metric_kitti.h"

#define TO_STREAM(PARAMETER_DESCRIPTION_, PARAMETER_, STREAM_) \
  STREAM_ << PARAMETER_DESCRIPTION_ << ": " << PARAMETER_ << std::endl;

#define TO_STREAM_RANGE(PARAMETER_DESCRIPTION_, PARAMETER_, PARAMETER_VECTOR_, STREAM_) \
  STREAM_ << PARAMETER_DESCRIPTION_ << ": " << PARAMETER_ << " [" << PARAMETER_VECTOR_.front() << ", " << PARAMETER_VECTOR_.back() << "]" << std::endl;

using namespace proslam;
int32_t main(int32_t argc_, char** argv_) {

  //ds simple input validation
  if (argc_ < 3) {
    std::cerr << "ERROR: invalid call - use ./tune_proslam_kitti <result_file.txt> <kitti_sequence_number_0> <kitti_sequence_number_1> .." << std::endl;
    std::cerr << "                     e.g. ./tune_proslam_kitti results.txt 00 03 05" << std::endl;
    return 0;
  }

  //ds grab result file name
  const std::string result_file_name = argv_[1];
  std::cerr << "result file: '" << result_file_name << "'" << std::endl;

  //ds generate secondary result file with "close" configurations
  const std::string secondary_result_file_name = "secondary-" + result_file_name;
  std::cerr << "secondary result file: '" << secondary_result_file_name << "'" << std::endl;

  //ds parse dataset indices
  std::vector<Identifier> sequence_numbers;
  while (sequence_numbers.size() < static_cast<std::vector<Identifier>::size_type>(argc_-2)) {
    sequence_numbers.push_back(std::stoull(argv_[sequence_numbers.size()+2]));
  }

  //ds objective values
  real error_translation_best = 1;
  real error_orientation_best = 1;

  //ds overall stats
  real error_translation_total = 0;
  real error_rotation_total    = 0;
  Count number_of_samples      = 0;

  //ds number of datasets  to check
  const Count number_of_datasets = sequence_numbers.size();

  //ds buffer ground truth poses for all datasets
  std::vector<MetricKITTI::PoseVector> poses_ground_truth(number_of_datasets);
  for (Index index = 0; index < number_of_datasets; ++index) {

    //ds generate file name
    char buffer_filename_ground_truth[32];
    std::sprintf(buffer_filename_ground_truth, "%02lu_gt.txt", sequence_numbers[index]);
    poses_ground_truth[index] = MetricKITTI::loadPoses(buffer_filename_ground_truth);
    std::printf("loaded ground truth poses for '%s' %4lu\n", buffer_filename_ground_truth, poses_ground_truth[index].size());
  }
  std::vector<real> error_translation_per_sequence(number_of_datasets);
  std::vector<real> error_orientation_per_sequence(number_of_datasets);

  //ds manual parameters
  const bool option_recover_landmarks               = true;
  const Count maximum_number_of_landmark_recoveries = 10;
  const bool enable_keypoint_binning                = true;
  const bool option_equalize_histogram              = false;

  const uint32_t number_of_detectors_vertical   = 2;
  const uint32_t number_of_detectors_horizontal = 2;

  const uint32_t minimum_track_length_for_landmark_creation = 2;
  const uint32_t minimum_number_of_landmarks_to_track       = 5;
  const std::string descriptor_type = "BRIEF-256";
  const real tunnel_vision_ratio    = 0.75;
  const real maximum_error_kernel   = 4*4;
  const uint32_t maximum_epipolar_search_offset_pixels = 0;
  const real maximum_reliable_depth_meters = 20.0;
  const real good_tracking_ratio         = 0.5;
  const Count minimum_number_of_inliers  = 100;
  const real error_delta_for_convergence = 1e-3;
  const real minimum_point_ratio_for_binning = 0.0;

  //ds automated parameters
  std::vector<real> target_number_of_keypoints_tolerances            = {0.1, 0.25};
  std::vector<real> detector_threshold_maximum_changes               = {0.1, 0.25, 0.5};

  std::vector<uint32_t> minimum_thresholds_distance_tracking_pixels  = {15};
  std::vector<uint32_t> maximum_thresholds_distance_tracking_pixels  = {50};

  std::vector<uint32_t> bin_sizes_pixels                             = {15};

  std::vector<uint32_t> minimum_descriptor_distances_tracking        = {25};
  std::vector<uint32_t> maximum_descriptor_distances_tracking        = {50, 55};

  std::vector<uint32_t> maximum_matching_distances_triangulation     = {50, 55, 60, 65, 70, 75};

  std::vector<double> dampings                                       = {1000};
  std::vector<uint32_t> detector_thresholds_minimum                  = {20};
  const uint32_t detector_threshold_maximum                          = 30;

  //ds start sampling for each parameter range
  std::printf("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
  uint64_t number_of_tested_parameters = 0;
  const uint64_t number_of_parameters_to_test = detector_threshold_maximum_changes.size()*
                                                target_number_of_keypoints_tolerances.size()*
                                                minimum_thresholds_distance_tracking_pixels.size()*
                                                maximum_thresholds_distance_tracking_pixels.size()*
                                                bin_sizes_pixels.size()*
                                                maximum_matching_distances_triangulation.size()*
                                                dampings.size()*
                                                minimum_descriptor_distances_tracking.size()*
                                                maximum_descriptor_distances_tracking.size()*
                                                detector_thresholds_minimum.size();
  std::cerr << "number of configurations to test: " << number_of_parameters_to_test << " .. good luck with that!" << std::endl;
  for (const real& detector_threshold_maximum_change: detector_threshold_maximum_changes) {
  for (const real& target_number_of_keypoints_tolerance: target_number_of_keypoints_tolerances) {
  for (const uint32_t& bin_size_pixels: bin_sizes_pixels) {
  for (const uint32_t& minimum_descriptor_distance_tracking: minimum_descriptor_distances_tracking) {
  for (const uint32_t& maximum_descriptor_distance_tracking: maximum_descriptor_distances_tracking) {
  for (const uint32_t& maximum_matching_distance_triangulation: maximum_matching_distances_triangulation) {
  for (const real& damping: dampings) {
  for (const uint32_t& minimum_threshold_distance_tracking_pixels: minimum_thresholds_distance_tracking_pixels) {
  for (const uint32_t& maximum_threshold_distance_tracking_pixels: maximum_thresholds_distance_tracking_pixels) {
  for (const uint32_t& detector_threshold_minimum: detector_thresholds_minimum) {
    try {

      //ds current result values
      real error_translation_accumulated = 0;
      real error_orientation_accumulated = 0;
      real average_fps                   = 0;

      //ds evaluate all datasets with the current parameter configuration (as of now all the txt_io datasets have to be directly in the root folder otherwise the image paths are broken)
      for (Index index = 0; index < number_of_datasets; ++index) {

        //ds get a fresh parameter collection with default parameters
        ParameterCollection* parameters = new ParameterCollection();

        //ds enable stereo mode
        parameters->setMode(CommandLineParameters::TrackerMode::RGB_STEREO);

        //ds use constant velocity motion model
        parameters->tracker_parameters->motion_model = Parameters::MotionModel::CONSTANT_VELOCITY;
        parameters->tracker_parameters->enable_landmark_recovery = option_recover_landmarks;
        parameters->tracker_parameters->maximum_number_of_landmark_recoveries = maximum_number_of_landmark_recoveries;
        parameters->tracker_parameters->minimum_number_of_landmarks_to_track  = minimum_number_of_landmarks_to_track;
        parameters->tracker_parameters->aligner->damping = damping;
        parameters->tracker_parameters->minimum_track_length_for_landmark_creation = minimum_track_length_for_landmark_creation;
        parameters->tracker_parameters->tunnel_vision_ratio = tunnel_vision_ratio;
        parameters->tracker_parameters->aligner->maximum_error_kernel = maximum_error_kernel;
        parameters->tracker_parameters->aligner->minimum_number_of_inliers = minimum_number_of_inliers;
        parameters->tracker_parameters->aligner->error_delta_for_convergence = error_delta_for_convergence;
        parameters->tracker_parameters->good_tracking_ratio = good_tracking_ratio;

        //ds disable relocalization (benchmarking odometry)
        parameters->command_line_parameters->option_disable_relocalization = true;
        parameters->command_line_parameters->option_drop_framepoints       = true;

        //ds manual parameters
        parameters->command_line_parameters->option_equalize_histogram  = option_equalize_histogram;
        parameters->command_line_parameters->option_recover_landmarks   = option_recover_landmarks;
        parameters->stereo_framepoint_generator_parameters->enable_keypoint_binning = enable_keypoint_binning;
        parameters->stereo_framepoint_generator_parameters->minimum_descriptor_distance_tracking = minimum_descriptor_distance_tracking;
        parameters->stereo_framepoint_generator_parameters->maximum_descriptor_distance_tracking = maximum_descriptor_distance_tracking;
        parameters->stereo_framepoint_generator_parameters->detector_threshold_minimum = detector_threshold_minimum;
        parameters->stereo_framepoint_generator_parameters->detector_threshold_maximum = detector_threshold_maximum;
        parameters->stereo_framepoint_generator_parameters->number_of_detectors_vertical   = number_of_detectors_vertical;
        parameters->stereo_framepoint_generator_parameters->number_of_detectors_horizontal = number_of_detectors_horizontal;
        parameters->stereo_framepoint_generator_parameters->descriptor_type                = descriptor_type;
        parameters->stereo_framepoint_generator_parameters->maximum_epipolar_search_offset_pixels = maximum_epipolar_search_offset_pixels;
        parameters->stereo_framepoint_generator_parameters->maximum_reliable_depth_meters = maximum_reliable_depth_meters;
        parameters->stereo_framepoint_generator_parameters->minimum_point_ratio_for_binning = minimum_point_ratio_for_binning;

        //ds tuning parameters
        parameters->stereo_framepoint_generator_parameters->detector_threshold_maximum_change = detector_threshold_maximum_change;
        parameters->stereo_framepoint_generator_parameters->target_number_of_keypoints_tolerance = target_number_of_keypoints_tolerance;
        parameters->stereo_framepoint_generator_parameters->minimum_projection_tracking_distance_pixels = minimum_threshold_distance_tracking_pixels;
        parameters->stereo_framepoint_generator_parameters->maximum_projection_tracking_distance_pixels = maximum_threshold_distance_tracking_pixels;
        parameters->stereo_framepoint_generator_parameters->maximum_matching_distance_triangulation = maximum_matching_distance_triangulation;
        parameters->stereo_framepoint_generator_parameters->bin_size_pixels = bin_size_pixels;

        //ds generate file name
        char dataset_file_name[32];
        std::sprintf(dataset_file_name, "%02lu.txt", sequence_numbers[index]);

        //ds set command line parameters
        parameters->command_line_parameters->dataset_file_name = dataset_file_name;

        //ds get a fresh proslam assembly (destroyed when leaving the for loop scope)
        SLAMAssembly* slam_system = new SLAMAssembly(parameters);

        //ds load cameras from message file
        slam_system->loadCamerasFromMessageFile();
        std::printf("[%06lu/%06lu] '%s' > ", number_of_tested_parameters, number_of_parameters_to_test-1, dataset_file_name);
        std::fflush(stdout);

        //ds start message playback - blocks until dataset is completed or aborted
        slam_system->playbackMessageFile();

        //ds obtain trajectory
        MetricKITTI::PoseVector poses(0);
        slam_system->writeTrajectory<float>(poses);

        //ds current errors
        if (poses.size() > 0) {
          const std::pair<float, float> error_translation_and_rotation = MetricKITTI::computeAverageErrorTranslationAndRotation(poses_ground_truth[index], poses);
          const real& error_translation = error_translation_and_rotation.first;
          const real& error_orientation    = error_translation_and_rotation.second;
          error_translation_total += error_translation;
          error_rotation_total    += error_orientation;
          ++number_of_samples;
          std::printf("R error (deg/100m): %7.4f (mean: %5.2f) | t error (%%): %7.4f (mean: %5.2f) fps: %3.0f landmarks: %3.0f tracks: %3.0f tracking ratio: %4.2f rec: %2lu\n",
                      100*error_orientation,
                      100*error_rotation_total/number_of_samples,
                      100*error_translation,
                      100*error_translation_total/number_of_samples,
                      slam_system->currentFPS(),
                      slam_system->averageNumberOfLandmarksPerFrame(),
                      slam_system->averageNumberOfTracksPerFrame(),
                      slam_system->meanTrackingRatio(),
                      slam_system->numberOfRecursiveRegistrations());
          error_translation_accumulated += error_translation;
          error_orientation_accumulated += error_orientation;
          average_fps                   += slam_system->currentFPS();
          error_translation_per_sequence[index] = error_translation;
          error_orientation_per_sequence[index] = error_orientation;
        } else {
          std::printf("no frames processed\n");
        }
        delete slam_system;
        delete parameters;
      }

      //ds check if we got a better result than the best so far
      const real error_translation_average = error_translation_accumulated/number_of_datasets;
      const real error_orientation_average = error_orientation_accumulated/number_of_datasets;
      if (error_translation_average < error_translation_best) {

        //ds if the gain is major or rotation improved - otherwise skip
        if ((error_translation_best-error_translation_average) > 0.0001 || error_orientation_average < error_orientation_best) {

          //ds update best
          error_translation_best = error_translation_average;
          error_orientation_best = error_orientation_average;
          std::printf("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
          std::printf("found new BEST rotation error (deg/100m): %7.4f | translation error (%%): %7.4f\n", 100*error_orientation_best, 100*error_translation_best);
          std::printf("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

          //ds open result file (appending to previous results)
          std::ofstream result_stream(result_file_name, std::ifstream::app);
          if (!result_stream.is_open() || !result_stream.good()) {
            std::cerr << "ERROR: unable to open result file: '" << result_file_name << "'" << std::endl;
            getchar();
          }

          //ds write header
          result_stream << std::endl;
          result_stream << "---------------------------------------------------------------" << std::endl;
          result_stream << " configuration identifier: " << number_of_tested_parameters << "/" << number_of_parameters_to_test-1 << std::endl;
          result_stream << "    translation error (%): " << 100*error_translation_best << std::endl;
          result_stream << "rotation error (deg/100m): " << 100*error_orientation_best << std::endl;
          result_stream << "              average FPS: " << average_fps/number_of_datasets << std::endl;
          result_stream << "          KITTI sequences: ";
          for (Index index = 0; index < number_of_datasets; ++index) {
            result_stream << "[" << sequence_numbers[index] << "] angular error: " << 100*error_orientation_per_sequence[index]
                                                              << " linear error: " << 100*error_translation_per_sequence[index] << std::endl;
            result_stream << "                           ";
          }
          result_stream << std::endl;
          result_stream << std::endl;
          result_stream << "parameters: " << std::endl;
          result_stream << std::endl;

          //ds manual parameters
          TO_STREAM("descriptor_type", descriptor_type, result_stream)
          TO_STREAM("parameters->stereo_tracker_parameters->enable_landmark_recovery", option_recover_landmarks, result_stream)
          TO_STREAM("parameters->stereo_tracker_parameters->maximum_number_of_landmark_recoveries", maximum_number_of_landmark_recoveries, result_stream)
          TO_STREAM("parameters->stereo_tracker_parameters->minimum_number_of_landmarks_to_track", minimum_number_of_landmarks_to_track, result_stream)
          TO_STREAM("parameters->stereo_framepoint_generator_parameters->enable_keypoint_binning", enable_keypoint_binning, result_stream)
          TO_STREAM("minimum_point_ratio_for_binning", minimum_point_ratio_for_binning, result_stream)
          TO_STREAM("parameters->command_line_parameters->option_equalize_histogram", option_equalize_histogram, result_stream)
          TO_STREAM_RANGE("minimum_descriptor_distance_tracking", minimum_descriptor_distance_tracking, minimum_descriptor_distances_tracking, result_stream)
          TO_STREAM_RANGE("maximum_descriptor_distance_tracking", maximum_descriptor_distance_tracking, maximum_descriptor_distances_tracking, result_stream)
          TO_STREAM("detector_threshold_minimum", detector_threshold_minimum, result_stream)
          TO_STREAM("detector_threshold_maximum", detector_threshold_maximum, result_stream)
          TO_STREAM("parameters->stereo_framepoint_generator_parameters->number_of_detectors_vertical", number_of_detectors_vertical, result_stream)
          TO_STREAM("parameters->stereo_framepoint_generator_parameters->number_of_detectors_horizontal", number_of_detectors_horizontal, result_stream)
          TO_STREAM("parameters->stereo_tracker_parameters->minimum_track_length_for_landmark_creation", minimum_track_length_for_landmark_creation, result_stream)
          TO_STREAM("maximum_reliable_depth_meters", maximum_reliable_depth_meters, result_stream)
          TO_STREAM("good_tracking_ratio", good_tracking_ratio, result_stream)
          TO_STREAM("good_number_of_tracked_points", minimum_number_of_inliers, result_stream)
          result_stream << std::endl;

          //ds tuning parameters
          TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->target_number_of_keypoints_tolerance", target_number_of_keypoints_tolerance, target_number_of_keypoints_tolerances, result_stream)
          TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->detector_threshold_maximum_change", detector_threshold_maximum_change, detector_threshold_maximum_changes, result_stream)
          TO_STREAM_RANGE("parameters->stereo_tracker_parameters->minimum_projection_tracking_distance_pixels", minimum_threshold_distance_tracking_pixels, minimum_thresholds_distance_tracking_pixels, result_stream)
          TO_STREAM_RANGE("parameters->stereo_tracker_parameters->maximum_projection_tracking_distance_pixels", maximum_threshold_distance_tracking_pixels, maximum_thresholds_distance_tracking_pixels, result_stream)
          TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->maximum_matching_distance_triangulation", maximum_matching_distance_triangulation, maximum_matching_distances_triangulation, result_stream)
          TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->bin_size_pixels", bin_size_pixels, bin_sizes_pixels, result_stream)
          TO_STREAM_RANGE("parameters->stereo_tracker_parameters->aligner->damping", damping, dampings, result_stream)
          result_stream << std::endl;

          //ds done
          result_stream.close();
        }

      //ds for configurations that achieve a similar error (+5%) to the current best and always have a lower rotation error
      } else if (error_translation_average < 1.05*error_translation_best && error_orientation_average < error_orientation_best) {

        //ds open result file (appending to previous results)
        std::ofstream result_stream(secondary_result_file_name, std::ifstream::app);
        if (!result_stream.is_open() || !result_stream.good()) {
          std::cerr << "ERROR: unable to open result file: '" << secondary_result_file_name << "'" << std::endl;
          getchar();
        }

        //ds write header
        result_stream << std::endl;
        result_stream << "---------------------------------------------------------------" << std::endl;
        result_stream << " configuration identifier: " << number_of_tested_parameters << "/" << number_of_parameters_to_test-1 << std::endl;
        result_stream << "    translation error (%): " << 100*error_translation_average << std::endl;
        result_stream << "rotation error (deg/100m): " << 100*error_orientation_average << std::endl;
        result_stream << "              average FPS: " << average_fps/number_of_datasets << std::endl;
        result_stream << "          KITTI sequences: ";
        for (Index index = 0; index < number_of_datasets; ++index) {
          result_stream << "[" << sequence_numbers[index] << "] angular error: " << 100*error_orientation_per_sequence[index]
                                                            << " linear error: " << 100*error_translation_per_sequence[index] << std::endl;
          result_stream << "                           ";
        }
        result_stream << std::endl;
        result_stream << std::endl;
        result_stream << "parameters: " << std::endl;
        result_stream << std::endl;

        //ds manual parameters
        TO_STREAM("descriptor_type", descriptor_type, result_stream)
        TO_STREAM("parameters->stereo_tracker_parameters->enable_landmark_recovery", option_recover_landmarks, result_stream)
        TO_STREAM("parameters->stereo_tracker_parameters->maximum_number_of_landmark_recoveries", maximum_number_of_landmark_recoveries, result_stream)
        TO_STREAM("parameters->stereo_tracker_parameters->minimum_number_of_landmarks_to_track", minimum_number_of_landmarks_to_track, result_stream)
        TO_STREAM("parameters->stereo_framepoint_generator_parameters->enable_keypoint_binning", enable_keypoint_binning, result_stream)
        TO_STREAM("minimum_point_ratio_for_binning", minimum_point_ratio_for_binning, result_stream)
        TO_STREAM("parameters->command_line_parameters->option_equalize_histogram", option_equalize_histogram, result_stream)
        TO_STREAM_RANGE("minimum_descriptor_distance_tracking", minimum_descriptor_distance_tracking, minimum_descriptor_distances_tracking, result_stream)
        TO_STREAM_RANGE("maximum_descriptor_distance_tracking", maximum_descriptor_distance_tracking, maximum_descriptor_distances_tracking, result_stream)
        TO_STREAM("parameters->stereo_framepoint_generator_parameters->detector_threshold_initial", detector_threshold_minimum, result_stream)
        TO_STREAM("detector_threshold_minimum", detector_threshold_minimum, result_stream)
        TO_STREAM("detector_threshold_maximum", detector_threshold_maximum, result_stream)
        TO_STREAM("parameters->stereo_framepoint_generator_parameters->number_of_detectors_vertical", number_of_detectors_vertical, result_stream)
        TO_STREAM("parameters->stereo_framepoint_generator_parameters->number_of_detectors_horizontal", number_of_detectors_horizontal, result_stream)
        TO_STREAM("parameters->stereo_tracker_parameters->minimum_track_length_for_landmark_creation", minimum_track_length_for_landmark_creation, result_stream)
        TO_STREAM("maximum_reliable_depth_meters", maximum_reliable_depth_meters, result_stream)
        TO_STREAM("good_tracking_ratio", good_tracking_ratio, result_stream)
        TO_STREAM("good_number_of_tracked_points", minimum_number_of_inliers, result_stream)
        result_stream << std::endl;

        //ds tuning parameters
        TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->target_number_of_keypoints_tolerance", target_number_of_keypoints_tolerance, target_number_of_keypoints_tolerances, result_stream)
        TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->detector_threshold_maximum_change", detector_threshold_maximum_change, detector_threshold_maximum_changes, result_stream)
        TO_STREAM_RANGE("parameters->stereo_tracker_parameters->minimum_projection_tracking_distance_pixels", minimum_threshold_distance_tracking_pixels, minimum_thresholds_distance_tracking_pixels, result_stream)
        TO_STREAM_RANGE("parameters->stereo_tracker_parameters->maximum_projection_tracking_distance_pixels", maximum_threshold_distance_tracking_pixels, maximum_thresholds_distance_tracking_pixels, result_stream)
        TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->maximum_matching_distance_triangulation", maximum_matching_distance_triangulation, maximum_matching_distances_triangulation, result_stream)
        TO_STREAM_RANGE("parameters->stereo_framepoint_generator_parameters->bin_size_pixels", bin_size_pixels, bin_sizes_pixels, result_stream)
        TO_STREAM_RANGE("parameters->stereo_tracker_parameters->aligner->damping", damping, dampings, result_stream)
        result_stream << std::endl;

        //ds done
        result_stream.close();
      }
      ++number_of_tested_parameters;
    }
    catch (std::out_of_range& exception_) {
      std::cerr << "caught exception: " << exception_.what() << std::endl;
      getchar();
    }
  }
  }
  }
  }
  }
  }
  }
  }
  }
  }

  //ds done
  return 0;
}
