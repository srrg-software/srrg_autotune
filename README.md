# SRRG AUTOTUNE
Dependencies:
- C++ 11 STL libraries
- srrg_proslam package (https://gitlab.com/srrg-software/srrg_proslam)

---
## Tuning example: ProSLAM in KITTI
Place the desired KITTI benchmark sequence files (.txt, .txt.d and _gt.txt) altogether in a shared folder (e.g. `datasets/kitti/tuning`) <br/>
This is best done by putting symbolic links to all files instead of copying them <br/>
Then launch the tuning program from the respective folder (e.g. `datasets/kitti/tuning`) using

    rosrun srrg_autotune tune_proslam_kitti results_kitti.txt 00 01 02

This will automatically start the evaluation for all parameter ranges on the KITTI sequences `00`, `01` and `02` <br/>
The current best parameter configuration is written at runtime to the file `results_kitti.txt`.

## Tuning example: ProSLAM in EuRoC
Place the desired EuRoC benchmark sequence files (.txt, .txt.d and `mav0/state_groundtruth_estimate0/data.csv`) altogether in a shared folder (e.g. `datasets/euroc/tuning`) <br/>
This is best done by putting symbolic links to all files instead of copying them <br/>
The symbolic link for `mav0/state_groundtruth_estimate0/data.csv` must be renamed to `sequence_name.csv` <br/>
Then launch the tuning program from the respective folder (e.g. `datasets/euroc/tuning`) using

    rosrun srrg_autotune tune_proslam_euroc results_euroc.txt MH_01_easy MH_02_easy

This will automatically start the evaluation for all parameter ranges on the KITTI sequences `MH_01_easy` and `MH_02_easy` <br/>
The current best parameter configuration is written at runtime to the file `results_euroc.txt`.
